import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { UsersComponent } from './users/users.component';
import { InserzioniComponent } from './inserzioni/inserzioni.component';
import { InserisciInserzioneComponent} from './inserisci-inserzione/inserisci-inserzione.component'
import { UserComponent } from './user/user.component';
import { UsersListComponent } from './users-list/users-list.component';
import { UsersNewComponent } from './users-new/users-new.component';
import { MessageComponent } from './message/message.component';

const routes: Routes = [
  { path: '', redirectTo: '/inserzioni', pathMatch: 'full' },
  { path: 'inserzioni', component: InserzioniComponent },
  { path: 'login', component: LoginComponent },
  { path: 'inserisciInserzione', component: InserisciInserzioneComponent },
  { path: 'users', component: UsersComponent },
  { path: 'user', component: UserComponent },
  {path : 'usersList' , component: UsersListComponent},
  {path:  'register', component: UsersNewComponent},
  {path:  'message/:insertByUser/:id', component: MessageComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { onSameUrlNavigation: 'reload' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
