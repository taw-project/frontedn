import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { Message } from '../message';
import { MessageService } from '../services/message.service';
import { SocketioService } from '../services/socketio.service';
import { UserService } from '../services/user.service';
@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {

  constructor(private router: Router, private _Activatedroute: ActivatedRoute, private ms: MessageService, private us: UserService,private sio: SocketioService) { }

  inserzioneId: string
  insertByUser: string
  messages: Message[] = []
  message: Message
  ngOnInit() {
    this._Activatedroute.paramMap.subscribe(params => {
      this.inserzioneId = params.get('id');
      this.insertByUser = params.get('insertByUser');
    });
    this.getInserzioniMessage(this.inserzioneId)
    this.sio.connect().subscribe( (m) => {
      this.getInserzioniMessage(this.inserzioneId)
    });
  }


  public getInserzioniMessage(inserzioneId) {
    this.ms.getInserzioniMessage(inserzioneId).subscribe(
      (messaggi) => {
        this.messages = messaggi
      }, (err) => {
        window.alert('Messaggi non ricevuti');
        console.log('Errore: ' + err);
      })
  }

  setEmpty() {
    this.message = {
      timestamp: new Date(),
      messageId: 0,
      mittente: "",
      destinatario:"",
      content: "",
      public: true,
      inserzioneId: Number(this.inserzioneId),
    }
  }

  post_message(text) {
    this.setEmpty()
    this.message.content = text
    this.message.timestamp = new Date()
    this.message.messageId= 0,
    this.message.mittente=this.us.username,
    this.message.destinatario= this.insertByUser,
    this.message.public= true,
    this.message.inserzioneId= Number(this.inserzioneId)
    this.ms.post_message(this.message).subscribe((m) => {
     }, (err) => {
      window.alert('Messaggi non inserito');
      console.log('Errore: ' + err);
    })
  }

}
