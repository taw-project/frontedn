export interface Message {
  messageId: Number,
    mittente: string,
    destinatario: string,
    content: string,
    public: boolean,
    inserzioneId: Number,
    timestamp: Date,
}
