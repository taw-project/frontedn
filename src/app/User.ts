export interface User {
    username: string;
    nome: string;
    cognome: string;
    password: string;
    role: string;
    regione: string,
    email: string,
    astePartecipate: [],
    asteVinte: [],
    asteVinteDaNotificare: [],
    asteFiniteDaNotificare:[],
}
