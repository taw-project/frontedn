import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Inject } from '@angular/core';
import { InserisciInserzioneService } from '../services/inserisci-inserzione.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-offerta',
  templateUrl: './offerta.component.html',
  styleUrls: ['./offerta.component.css']
})
export class OffertaComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private ins: InserisciInserzioneService, private router: Router,public dialogRef: MatDialogRef<OffertaComponent>) { }



  ngOnInit() {
    console.log(this.data)
  }

  public aumentaPrezzo(value) {
    this.ins.put_offerta(this.data.inserzione.inserzioneId, value).subscribe((o) => {
      this.router.navigateByUrl('/inserzioni');
      this.dialogRef.close()
      window.location.reload()
    }, (err) => {
      window.alert('Offerta non inserita');
      console.log('Errore: ' + err);
    })
  }

}
