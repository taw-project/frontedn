import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { Router } from '@angular/router';
import { Inserzione } from '../Inserzione';
import { UserService } from './user.service';
@Injectable({
  providedIn: 'root'
})
export class InserzioneService {
  constructor(private http: HttpClient, private us: UserService) { }
  public token = '';
  public url = 'http://localhost:3000';

  /*get_inserzioni(): Observable<Inserzione[]> {
      const options = {
          headers: new HttpHeaders({
              'cache-control': 'no-cache',
              'Content-Type': 'application/json',
          })
      };
      return this.http.get<Inserzione[]>(this.url + '/inserzioni', options).pipe(
          map(o => o.map((sp): Inserzione => ({ // IProduct specified here ensures we get excess property checks
              inserzioneId: sp.inserzioneId , // number in server interface, map to string 
              durata: sp.durata,
              prezzoRiserva: sp.prezzoRiserva,
              prezzoPartenza: sp.prezzoPartenza,
              prezzoAttuale: sp.prezzoAttuale,
              insertByUser: sp.insertByUser,
              userWinner: sp.userWinner,
              active: sp.active,
              bookId: sp.bookId,
          })))
        );
  }*/
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        'body was: ' + JSON.stringify(error.error));
    }
    // return an ErrorObservable with a user-facing error message
    return throwError('Something bad happened; please try again later.');
  }

  private create_options(params = {}) {
    return {
      headers: new HttpHeaders({
        authorization: 'Bearer ' + this.us.get_token(),
        'cache-control': 'no-cache',
        'Content-Type': 'application/json'
      }),
      params: new HttpParams({ fromObject: params })
    };
  }

  get_inserzioni(): Observable<Inserzione[]> {
    return this.http.get<Inserzione[]>(this.url + '/inserzioni', this.create_options()).pipe(
      catchError(this.handleError)
    );
  }

  get_inserzioni_active(): Observable<Inserzione[]> {
    return this.http.get<Inserzione[]>(this.url + '/inserzioni/active', this.create_options()).pipe(
      catchError(this.handleError)
    );
  }

  get_inserzione_from_id(idInserzione: Number): Observable<Inserzione> {
    return this.http.get<Inserzione>(this.url + '/inserzioni/' + idInserzione, this.create_options()).pipe(
      catchError(this.handleError)
    );
  }

  get_inserzioni_win_by_user(username: String): Observable<Inserzione[]> {
    return this.http.get<Inserzione[]>(this.url + '/inserzioni/winbyuser/' + username, this.create_options()).pipe(
      
      catchError(this.handleError)
    );
  }

  get_inserzioni_da_notificare(username: String): Observable<Inserzione[]> {
    return this.http.get<Inserzione[]>(this.url + '/inserzioni/winbyusernotnotify/' + username, this.create_options()).pipe(
      catchError(this.handleError)
    );
  }

  delete_inserzione(inserzione): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        Authorization: 'Bearer ' + this.us.get_token(),
        'cache-control': 'no-cache',
        'Content-Type': 'application/json',
      }),
    };

    return this.http.delete(this.url + '/inserzioni/' + inserzione, options).pipe(
      tap((data) => {
        console.log(JSON.stringify(data));
      })
    );
  }

  getInserzioniByUser(username: string): Observable<Inserzione[]> {
    return this.http.get<Inserzione[]>(this.url + '/inserzioni/user/' + username, this.create_options()).pipe(
      catchError(this.handleError)
    );
  }

  getInserzioniPartecipateByUser(username: string): Observable<Inserzione[]> {
    return this.http.get<Inserzione[]>(this.url + '/inserzioni/partecipate/' + username, this.create_options()).pipe(
      catchError(this.handleError)
    );
  }

  getInserzioniNoRaggiuntaRiserva(): Observable<Inserzione[]> {
    return this.http.get<Inserzione[]>(this.url + '/inserzioni/notActive/notUserWinner', this.create_options()).pipe(
      catchError(this.handleError)
    );
  }

}