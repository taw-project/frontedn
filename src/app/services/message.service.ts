import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Message } from '../message';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class MessageService {
  
  constructor(private http: HttpClient, private us : UserService) { }
  public url = 'http://localhost:3000';

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        'body was: ' + JSON.stringify(error.error));
    }
    // return an ErrorObservable with a user-facing error message
    return throwError('Something bad happened; please try again later.');
  }

  private create_options(params = {}) {
    return {
      headers: new HttpHeaders({
        authorization: 'Bearer ' + this.us.get_token(),
        'cache-control': 'no-cache',
        'Content-Type': 'application/json'
      }),
      params: new HttpParams({ fromObject: params })
    };
  }

  getInserzioniMessage(inserzioneId): Observable<Message[]> {
    return this.http.get<Message[]>(this.url + '/inserzione/message/'+ inserzioneId, this.create_options()).pipe(
      catchError(this.handleError)
    );
  }



  post_message(mess: Message): Observable<Message> {
    return this.http.post<Message>(this.us.url + '/message', mess, this.create_options()).pipe(
      catchError(this.handleError)
    );
  }
}
