import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { tap, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { Router } from '@angular/router';
import { User } from '../User';
import { location } from '../app.component';
const jwtdecode = require('jwt-decode');

// import { ErrorObservable } from 'rxjs/observable/ErrorObservable';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient, private router: Router) { }
  public token = '';
  public username = ''
  public url = 'http://localhost:3000';

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        'body was: ' + JSON.stringify(error.error));
    }
    // return an ErrorObservable with a user-facing error message
    return throwError('Something bad happened; please try again later.');
  }

  login(username: string, password: string): Observable<any> {
    console.log('Login: ' + username + ' ' + password);
    const options = {
      headers: new HttpHeaders({
        authorization: 'Basic ' + btoa(username + ':' + password),
        // 'cache-control': 'no-cache',
        // 'Content-Type': 'application/x-www-form-urlencoded'
      })
    };

    return this.http.get(this.url + '/login', options).pipe(
      tap((data) => {
        this.token = data.token;
        this.username = username
          localStorage.setItem('token', this.token);
          localStorage.setItem('username', this.username)
      })
    );
  }

  renew(): Observable<any> {
    const tk = localStorage.getItem('token');

    if (!tk || tk.length < 1) {
      return throwError({ error: { errormessage: 'No token found in local storage' } });
    }

    const options = {
      headers: new HttpHeaders({
        Authorization: 'Bearer ' + tk,
        'cache-control': 'no-cache',
        'Content-Type': 'application/json',
      })
    };

    //console.log(location + ' user.service');
    return this.http.get(this.url + '/renew', options).pipe(
      tap((data) => {
        this.token = data.token;
        this.username = localStorage.username
        localStorage.setItem('token', this.token);
      })
    );
  }

  logout() {
    console.log('Logging out');
    this.token = '';
    this.username ='';
    localStorage.setItem('token', this.token);
    localStorage.setItem('username',this.username)
    this.router.navigateByUrl('/login');
  }



  registerStudent(user: User): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        'cache-control': 'no-cache',
        'Content-Type': 'application/json',
      })
    };

    return this.http.post<User>(this.url + '/user/student', user, options).pipe(
      /*tap((data) => {
        console.log(JSON.stringify(data));
      })*/
    );
  }

  registerModerator(user: User): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        Authorization: 'Bearer ' + this.get_token(),
        'cache-control': 'no-cache',
        'Content-Type': 'application/json',
      })
    };

    return this.http.post<User>(this.url + '/user/moderatore', user, options).pipe(
      /*tap((data) => {
        console.log(JSON.stringify(data));
      })*/
    );
  }

  delete_user(user): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        Authorization: 'Bearer ' + this.get_token(),
        'cache-control': 'no-cache',
        'Content-Type': 'application/json',
      }),
    };

    return this.http.delete(this.url + '/user/'+ user, options).pipe(
      tap((data) => {
        console.log(JSON.stringify(data));
      })
    );
  }

  get_users(): Observable<User[]> {
    const options = {
      headers: new HttpHeaders({
        Authorization: 'Bearer ' + localStorage.token,
        'cache-control': 'no-cache',
        'Content-Type': 'application/json',
      })
    };
    return this.http.get<User[]>(this.url + '/users', options).pipe(
      catchError(this.handleError)
    );
  }

  get_token() {
    return this.token;
  }

  get_username() {
    var a = jwtdecode(this.token).username
    return a;
  }

  get_role() {
    if (localStorage.token){
      var a = (jwtdecode(localStorage.token).role)
      return a;
    }
    return undefined
  }

  get_role_with_token(tok) {
    var a = (jwtdecode(tok).role)
    return a;
  }

  get_aste_da_notificare(username: String): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        Authorization: 'Bearer ' + this.get_token(),
        'cache-control': 'no-cache',
        'Content-Type': 'application/json',
      })
    };
    return this.http.get<User[]>(this.url + '/user/' + username + "/asteVinteDaNotificare", options).pipe(
      tap((data) => {
      })
    );
  }

  get_aste_finite_da_notificare(username: String): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        Authorization: 'Bearer ' + this.get_token(),
        'cache-control': 'no-cache',
        'Content-Type': 'application/json',
      })
    };
    return this.http.get<User[]>(this.url + '/user/' + username + "/asteFiniteDaNotificare", options).pipe(
      tap((data) => {
      })
    );
  }

  get_user(username): Observable<User> {
    const options = {
      headers: new HttpHeaders({
        Authorization: 'Bearer ' + this.get_token(),
        'cache-control': 'no-cache',
        'Content-Type': 'application/json',
      })
    };
    return this.http.get<User>(this.url + '/user/' + username, options).pipe(
      catchError(this.handleError)
    );
  }

  put_user(user: any): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        Authorization: 'Bearer ' + this.get_token(),
        'cache-control': 'no-cache',
        'Content-Type': 'application/json',
      })
    };

    return this.http.put<any>(this.url + '/user', user, options).pipe(
      catchError(this.handleError)
    )
  }

  emptyNotify(user: string): Observable<any>{
    const options = {
      headers: new HttpHeaders({
        Authorization: 'Bearer ' + this.get_token(),
        'cache-control': 'no-cache',
        'Content-Type': 'application/json',
      })
    };
    return this.http.put<any>(this.url + '/user/' + user +'/emptyNotify', options).pipe(
      catchError(this.handleError)
    )
  }

}
