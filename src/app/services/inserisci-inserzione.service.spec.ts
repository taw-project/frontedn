import { TestBed } from '@angular/core/testing';

import { InserisciInserzioneService } from './inserisci-inserzione.service';

describe('InserisciInserzioneService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InserisciInserzioneService = TestBed.get(InserisciInserzioneService);
    expect(service).toBeTruthy();
  });
});
