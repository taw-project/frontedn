import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { Router } from '@angular/router';
import { Inserzione } from '../Inserzione';
import { UserService } from './user.service';
import { indexDebugNode } from '@angular/core/src/debug/debug_node';

@Injectable({
  providedIn: 'root'
})
export class InserisciInserzioneService {

  constructor(private http: HttpClient, private router: Router, private us: UserService) { }
  public token = '';
  public url = 'http://localhost:3000';

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        'body was: ' + JSON.stringify(error.error));
    }
    // return an ErrorObservable with a user-facing error message
    return throwError('Something bad happened; please try again later.');
  }

  private create_options(params = {}) {
    return {
      headers: new HttpHeaders({
        authorization: 'Bearer ' + this.us.get_token(),
        'cache-control': 'no-cache',
        'Content-Type': 'application/json'
      }),
      params: new HttpParams({ fromObject: params })
    };
  }

  post_inserzione(ins: Inserzione): Observable<Inserzione> {
    return this.http.post<Inserzione>(this.us.url + '/inserzioni', ins, this.create_options()).pipe(
      catchError(this.handleError)
    );
  }

  put_offerta(idInserzione: Number, prezzo: Number): Observable<Inserzione> {
    return this.http.put<Inserzione>(this.us.url + '/inserzioni/' + idInserzione + '/offerta/' + prezzo, {}, this.create_options()).pipe(
      catchError(this.handleError)
    );
  }

  put_inserzione(ins: Inserzione): Observable<Inserzione> {
    return this.http.put<Inserzione>(this.us.url + '/inserzioni', ins, this.create_options()).pipe(
      catchError(this.handleError)
    );
  }

}