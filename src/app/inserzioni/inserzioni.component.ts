import { Component, Input, OnInit, AfterViewInit } from '@angular/core';
import { Inserzione } from '../Inserzione';
import { InserzioneService } from '../services/inserzioni.service';
import { UserService } from '../services/user.service';
import { MatDialog } from '@angular/material'
import { OffertaComponent } from '../offerta/offerta.component';
import { ModificaInserzioneComponent } from '../modifica-inserzione/modifica-inserzione.component';
import { SocketioService } from '../services/socketio.service';
import 'rxjs/add/observable/interval';
import { Router } from '@angular/router';

import { ViewChild } from '@angular/core';
import { MatSort, MatSortable } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { FormControl } from '@angular/forms';


@Component({
  selector: 'app-inserzioni',
  templateUrl: './inserzioni.component.html',
  styleUrls: ['./inserzioni.component.css']

})
export class InserzioniComponent implements OnInit {
  displayedColumns: string[] = [];
  dataSource = new MatTableDataSource<Inserzione>();
  titleFilter = new FormControl('');
  universityFilter = new FormControl('');
  courseFilter = new FormControl('');
  userWinnerFilter = new FormControl('');
  insertByUserFilter = new FormControl('');
  filterValues = {
    title: '',
    university: '',
    course: '',
    userWinner: '',
    insertByUser: '',
  };
  @ViewChild(MatSort) sort: MatSort;

  @Input()
  inserzione: Inserzione;
  inserzioneId: Number;
  inserzioni: Inserzione[] = []
  showButtonOfferta: boolean
  role = ''
  constructor(private is: InserzioneService, private us: UserService, public dialog: MatDialog, private sio: SocketioService, private router: Router) {
    // Assign the data to the data source for the table to render
  }

  ngOnInit(): void {
    this.role = this.us.get_role()
    this.showButtonOfferta = !localStorage.token ? false : true;
    this.getDisplayedColumns()
    this.get_inserzioni_active()
    /*if (this.role != '') {
      this.sio.connect()
    }*/
    this.titleFilter.valueChanges
      .subscribe(
        title => {
          this.filterValues.title = title;
          this.dataSource.filter = JSON.stringify(this.filterValues);
        }
      )

    this.universityFilter.valueChanges
      .subscribe(
        university => {
          this.filterValues.university = university;
          this.dataSource.filter = JSON.stringify(this.filterValues);
        }
      )

    this.courseFilter.valueChanges
      .subscribe(
        course => {
          this.filterValues.course = course;
          this.dataSource.filter = JSON.stringify(this.filterValues);
        }
      )

    this.insertByUserFilter.valueChanges
      .subscribe(
        insertByUser => {
          this.filterValues.insertByUser = insertByUser;
          this.dataSource.filter = JSON.stringify(this.filterValues);
        }
      )

    this.userWinnerFilter.valueChanges
      .subscribe(
        userWinner => {
          this.filterValues.userWinner = userWinner;
          this.dataSource.filter = JSON.stringify(this.filterValues);
        }
      )

  }

  createFilter(): (data: any, filter: string) => boolean {
    let filterFunction = function (data, filter): boolean {
      let searchTerms = JSON.parse(filter);
      return data.book.title.toLowerCase().indexOf(searchTerms.title) !== -1
        && data.book.university.toLowerCase().indexOf(searchTerms.university) !== -1
        && data.book.course.toLowerCase().indexOf(searchTerms.course) !== -1
        && data.userWinner.toLowerCase().indexOf(searchTerms.userWinner) !== -1
        && data.insertByUser.toLowerCase().indexOf(searchTerms.insertByUser) !== -1
    }
    return filterFunction;
  }




  public get_inserzioni_active() {
    if (!localStorage.token) {
      this.is.get_inserzioni().subscribe(
        (inserzioni) => {
          this.inserzioni = inserzioni
          this.dataSource = new MatTableDataSource(inserzioni);
          this.sort.sort(({ id: 'active', start: 'desc' }) as MatSortable);
          this.dataSource.sort = this.sort
          this.dataSource.filterPredicate = this.createFilter();

        }, (err) => {
          window.alert('Inserzioni non ricevute');
        })
    }
    else {
      this.us.renew().subscribe(() => {
      })
      this.role = this.us.get_role_with_token(localStorage.token)
      this.is.get_inserzioni().subscribe(
        (inserzioni) => {
          this.dataSource = new MatTableDataSource(inserzioni);
          this.inserzioni = inserzioni
          this.sort.sort(({ id: 'active', start: 'desc' }) as MatSortable);
          this.dataSource.filterPredicate = this.createFilter();

          this.dataSource.sort = this.sort
        }, (err) => {
          window.alert('Inserzioni non ricevute');
        })
    }
  }

  openDialogOfferta(inserzione) {
    this.dialog.open(OffertaComponent, { data: { inserzione: inserzione } })
  }

  openDialogModifica(inserzione) {
    this.dialog.open(ModificaInserzioneComponent, { data: { inserzione: inserzione } })
  }

  cancella(inserzione) {
    this.is.delete_inserzione(inserzione.inserzioneId).subscribe(
      (o) => {
        window.location.reload()
      }
    );
  }

  getDisplayedColumns() {
    if (this.showButtonOfferta === true) {
      if (this.role !== 'MODERATORE') {
        this.displayedColumns = ['active', 'title', 'university', 'course', 'prezzoPartenza', 'prezzoAttuale', 'prezzoRiserva', 'data', 'ora', 'userWinner', 'insertByUser', 'dettagli', 'messaggi'];

      }
      else {

        this.displayedColumns = ['active','title', 'university', 'course', 'prezzoPartenza', 'prezzoAttuale', 'prezzoRiserva', 'data', 'ora',  'userWinner', 'insertByUser', 'dettagli'];
      }
    }
    else {
      this.displayedColumns = [ 'active', 'title', 'university', 'course', 'prezzoPartenza', 'prezzoAttuale', 'prezzoRiserva', 'data', 'ora', 'userWinner', 'insertByUser'];

    }
  }


}


