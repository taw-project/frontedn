import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  showButtonLogin: boolean;
  showButtonInserzioni : boolean
  isModerator : boolean
  constructor(private us: UserService, private router: Router) {}
  ngOnInit() {
    if (localStorage.token) {
      this.us.renew().subscribe(() => {
      })
    }
    this.showButtonLogin = localStorage.token === '' ? true : false;
    this.showButtonInserzioni = this.router.url == "/inserzioni" ? true : false
    this.isModerator = this.us.get_role() === 'MODERATORE' ? true : false
  }

  public go_to_inserisci() {
    this.router.navigateByUrl("/inserisciInserzione");
  }

  public go_to_inserzioni() {
    this.router.navigateByUrl("/inserzioni");
  }

  public logout() {
    this.us.logout();
  }

  public go_to_profilo(){
    this.router.navigateByUrl("/user");
  }

  public go_to_listaUtenti(){
    this.router.navigateByUrl("/usersList");
  }

  public go_to_newUser(){
    this.router.navigateByUrl("/register");
  }

}
