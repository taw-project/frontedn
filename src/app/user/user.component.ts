import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Inserzione } from '../Inserzione';
import { InserzioneService } from '../services/inserzioni.service';
import { UserService } from '../services/user.service';
import { User } from '../User';
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  //@Input() notification: Notification
  @Input('daNotificare') daNotificare: Number[];
  displayedColumnsDaNotificare: string[] = ['title', 'university', 'course', 'prezzoAttuale',];
  displayedColumnsInserite: string[] = ['title', 'university', 'course', 'prezzoAttuale', 'active', 'userWinner'];
  displayedColumnsRiserva: string[] = ['title', 'university', 'course', 'prezzoAttuale', 'prezzoRiserva'];

  role = ''
  username = ''

  inserzioniVinte: Inserzione[] = []
  inserzioniDaNotificare: Inserzione[] = []
  inserzioniInserite: Inserzione[] = []
  inserzioniPartecipate: Inserzione[] = []
  inserzioniAttive: Inserzione[] = []
  inserzioniNonRaggiunte: Inserzione[] = []
  numeroInserzioniAttive = 0
  panelDatiUtente = false
  user = {
    nome: "",
    cognome: "",
    password: "",
    username: "",
    email: "",
    regione: "",
  }
  userData: User

  constructor(private us: UserService, private router: Router, private is: InserzioneService) {
    if (this.us.get_token() === '') {
      this.us.token = localStorage.token
      this.role = this.us.get_role()
      this.username = this.us.get_username()
    }
  }
  ngOnInit() {
    this.getUserData()
  }

  getUserData() {
    this.role = this.us.get_role()
    this.username = this.us.get_username()
    this.us.get_user(this.username).subscribe(
      (userData) => {
        this.userData = userData
      }
    );
    if (this.role === "STUDENTE") {
      this.is.get_inserzioni_win_by_user(this.username).subscribe(
        (inserzioniVinte) => {
          console.log(this.username)
          this.inserzioniVinte = inserzioniVinte
        }
      );
      this.is.get_inserzioni_da_notificare(this.username).subscribe(
        (inserzioniDaNotificare) => {
          this.inserzioniDaNotificare = inserzioniDaNotificare
        }
      )
      this.is.getInserzioniByUser(this.username).subscribe(
        (inserzioniInserite) => {
          this.inserzioniInserite = inserzioniInserite
        }
      )

      this.is.getInserzioniPartecipateByUser(this.username).subscribe(
        (inserzioniPartecipate) => {
          this.inserzioniPartecipate = inserzioniPartecipate
        }
      )

      this.us.emptyNotify(this.username).subscribe((o) => {
        //this.panelDatiUtente = false
      })
    }
    else {
      this.is.get_inserzioni_active().subscribe(
        (inserzioni) => {
          this.inserzioniAttive = inserzioni
          this.numeroInserzioniAttive = inserzioni.length
        }
      );
      this.is.getInserzioniNoRaggiuntaRiserva().subscribe(
        (inserzioni) => {
          this.inserzioniNonRaggiunte = inserzioni
        }
      ) 
    }
  }

  public modifica(nome, cognome, password, regione, email) {
    this.user.username = this.username
    this.user.nome = nome
    this.user.cognome = cognome
    this.user.password = password
    this.user.email = email
    this.user.regione = regione
    this.us.put_user(this.user).subscribe((o) => {
      this.panelDatiUtente = false
    }, (err) => {
      window.alert('Utente non modificato');
      console.log('Errore: ' + err);
    })

  }


}
