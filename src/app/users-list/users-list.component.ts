import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';
import { User } from '../User';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})

export class UsersListComponent implements OnInit {
  displayedColumns: string[] = ['username', 'role', 'email', 'regione', 'elimina'];
  users: User[] = [];
  role: ''
  constructor(private us: UserService, private router: Router) { }

  ngOnInit() {
    this.get_users();
  }

  public get_users() {
    if (!localStorage.token) {
      this.us.get_users().subscribe(
        (users) => {
          this.users = users
        }, (err) => {
          window.alert('Utenti non ricevuti');
          console.log('Errore: ' + err);
        })
    }
    else {
      this.role = this.us.get_role_with_token(localStorage.token)
      this.us.renew().subscribe(() => {
      })
      this.us.get_users().subscribe(
        (users) => {
          this.users = users
        }, (err) => {
          window.alert('Utenti non ricevuti');
          console.log('Errore: ' + err);
        })
    }
  }

  public delete_user(username) {
    this.us.delete_user(username).subscribe(() => {
      window.location.reload()
    })
  }

}
