export interface Book {
    bookId: number;
    title: string,
    university: string,
    course: string,
    user: string,
    inserzioneId: Number,
}

export interface Inserzione {
    inserzioneId: number,
    data: string,
    ora: string,
    prezzoRiserva: number,
    prezzoPartenza: number,
    prezzoAttuale: number,
    insertByUser: string,
    userWinner: string,
    active: boolean,
    bookId: number,
    book : Book
} 