import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';
import { User } from '../User';

@Component({
  selector: 'app-users-new',
  templateUrl: './users-new.component.html',
  styleUrls: ['./users-new.component.css']
})
export class UsersNewComponent implements OnInit {

  users: User[] = [];

  user: User
  role
  constructor(private us: UserService, private router: Router) { }

  setEmpty() {
    this.user = {
      username: "",
      password: "",
      role: "",
      nome: "",
      cognome: "",
      regione: "",
      email: "",
      astePartecipate: [],
      asteVinte: [],
      asteVinteDaNotificare: [],
      asteFiniteDaNotificare:[],
    }
  }

  ngOnInit() {
    this.role = this.us.get_role()
    console.log(this.role)
  }

  public register(username,password,nome,cognome,email,regione){
    
    this.setEmpty()
    this.user.username = username
    this.user.password = password
    this.user.role = this.us.get_role() === "MODERATORE" ? "MODERATORE" : "STUDENTE"
    this.user.nome = nome
    this.user.cognome = cognome
    this.user.regione = regione
    this.user.email = email
    if(this.us.get_role() === "MODERATORE"){
      this.us.registerModerator(this.user).subscribe((o) =>{
        this.router.navigateByUrl('/user');
      })
    }
    else{
      this.us.registerStudent(this.user).subscribe((o) =>{
        this.router.navigateByUrl('/login');
      }, (err) => {
        window.alert('Utenti non inserito');
        console.log('Errore: ' + err);
      })
    }
  }


}
