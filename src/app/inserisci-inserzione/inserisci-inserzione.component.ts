import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../services/user.service';
import { Inserzione, Book } from '../Inserzione';
import { InserzioneService } from '../services/inserzioni.service';
import { InserisciInserzioneService } from '../services/inserisci-inserzione.service';
@Component({
  selector: 'app-inserisci-inserzione',
  templateUrl: './inserisci-inserzione.component.html',
  styleUrls: ['./inserisci-inserzione.component.css']
})
export class InserisciInserzioneComponent implements OnInit {

  days: Number[] = [
    1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31
  ];

  months: Number[] = [
    1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12
  ];

  years: Number[] = [2021, 2022, 2023]

  hours: Number[] = [
    0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23
  ];

  minutes: Number[] = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59]

  @Input()
  inserzione: Inserzione
  book: Book

  constructor(private us: UserService, private router: Router, private ins: InserisciInserzioneService) {
    if (this.us.get_token() === '') {
      this.router.navigateByUrl('/inserzioni');
    }
  }



  ngOnInit() {
  }

  set_empty() {
    this.book = {
      bookId: 0,
      title: "",
      university: "",
      course: "",
      user: this.us.username,
      inserzioneId: 0,
    };

    this.inserzione = {
      inserzioneId: 0,
      data: "",
      ora: "",
      prezzoRiserva: 0,
      prezzoPartenza: 0,
      prezzoAttuale: 0,
      insertByUser: this.us.username,
      userWinner: "",
      active: true,
      bookId: 0,
      book: this.book
    }
  }

  public inserisci(titolo, università, corso, data, month, year, ora, minuti, prezzoRiserva, prezzoPartenza) {
    var date = new Date()
    var insert = true;
    if (month <= date.getMonth() + 1) {
      if (data < date.getDate()) {
        alert("Impossibile inserire una data precedente")
        insert = false;
      }
    }
    if (Number(month) === date.getMonth() + 1 && Number(data) === date.getDate()) {
      if (ora <= Number(date.getHours())) {
        if (minuti <= Number(date.getMinutes())) {
          alert("Impossibile inserire un orario precedente")
          insert = false;
        }
      }
    }
    if (insert && titolo !== undefined && università !== undefined && corso !== undefined && data !== undefined && month !== undefined && year !== undefined && ora !== undefined && minuti !== undefined && prezzoRiserva !== undefined && prezzoPartenza !== undefined) {
      this.set_empty()
      this.book.title = titolo
      this.book.university = università
      this.book.course = corso
      this.inserzione.book = this.book
      data = data < 10 ? "0" + data : data
      month = month < 10 ? "0" + month : month
      year = year < 10 ? "0" + year : year
      this.inserzione.data = data + "/" + month + "/" + year
      minuti = minuti < 10 ? "0" + minuti : minuti
      ora = ora < 10 ? "0" + ora : ora
      this.inserzione.ora = ora + ":" + minuti
      this.inserzione.prezzoRiserva = Number(prezzoRiserva)
      this.inserzione.prezzoAttuale = Number(prezzoPartenza)
      this.inserzione.prezzoPartenza = Number(prezzoPartenza)
      this.inserzione.active = true

      this.ins.post_inserzione(this.inserzione).subscribe((o) => {
        this.router.navigateByUrl('/inserzioni');
      }, (err) => {
        window.alert('Inserzioni non isnerita');
      });
    }
    else {
      alert("Possibili dati mancanti")
    }

  }
}


