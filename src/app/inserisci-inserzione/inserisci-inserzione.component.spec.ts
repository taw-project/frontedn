import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InserisciInserzioneComponent } from './inserisci-inserzione.component';

describe('InserisciInserzioneComponent', () => {
  let component: InserisciInserzioneComponent;
  let fixture: ComponentFixture<InserisciInserzioneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InserisciInserzioneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InserisciInserzioneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
