import { Component, Inject, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Inserzione } from '../Inserzione';
import { InserisciInserzioneService } from '../services/inserisci-inserzione.service';
import { InserzioneService } from '../services/inserzioni.service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-modifica-inserzione',
  templateUrl: './modifica-inserzione.component.html',
  styleUrls: ['./modifica-inserzione.component.css']
})
export class ModificaInserzioneComponent implements OnInit {

  @Input()
  inserzione: Inserzione;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
    private is: InserzioneService, private us: UserService,
    public dialogRef: MatDialogRef<ModificaInserzioneComponent>,
    private ins: InserisciInserzioneService,
    private router: Router) {
  }
  ngOnInit() {
    this.inserzione = this.data.inserzione

  }

  public modifica(titolo, università, corso, data, ora, prezzoRiserva, prezzoPartenza) {
    this.inserzione.book.title = titolo
    this.inserzione.book.university = università
    this.inserzione.book.course = corso
    this.inserzione.data = data
    this.inserzione.ora = ora
    this.inserzione.prezzoRiserva = Number(prezzoRiserva)
    this.inserzione.prezzoAttuale = Number(prezzoPartenza)
    this.inserzione.prezzoPartenza = Number(prezzoPartenza)
    this.inserzione.active = true
    this.ins.put_inserzione(this.inserzione).subscribe((o) => {
      this.dialogRef.close()
      this.router.navigateByUrl('/inserzioni');
    }, (err) => {
      window.alert('Inserzione non modificata');
      console.log('Errore: ' + err);
    })
  }

}
