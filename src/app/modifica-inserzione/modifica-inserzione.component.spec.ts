import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModificaInserzioneComponent } from './modifica-inserzione.component';

describe('ModificaInserzioneComponent', () => {
  let component: ModificaInserzioneComponent;
  let fixture: ComponentFixture<ModificaInserzioneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModificaInserzioneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModificaInserzioneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
