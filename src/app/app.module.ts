import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { LoginComponent } from './login/login.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UsersComponent } from './users/users.component';
import { UsersNewComponent } from './users-new/users-new.component';
import { UsersListComponent } from './users-list/users-list.component';
import { InserzioniComponent } from './inserzioni/inserzioni.component';
import { InserisciInserzioneComponent } from './inserisci-inserzione/inserisci-inserzione.component';
import { OffertaComponent } from './offerta/offerta.component';
import {
  MatExpansionModule,  
  MatDialogModule, 
  MatButtonModule,
  MatTableModule,
  MatIconModule,
  MatCheckbox,
  MatCheckboxModule,
  MatLabel,
  MatSortModule,
  MatFormFieldModule,
  MatPaginatorModule,
  MatInputModule,
  MatProgressSpinnerModule,
  MatOptionModule,
  MatSelectModule
} from '@angular/material';
import { ModificaInserzioneComponent } from './modifica-inserzione/modifica-inserzione.component';
import { NotificheComponent } from './notifiche/notifiche.component';
import { UserComponent } from './user/user.component';
import { MessageComponent } from './message/message.component'
@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    UsersComponent,
    UsersNewComponent,
    UsersListComponent,
    InserzioniComponent,
    InserisciInserzioneComponent,
    OffertaComponent,
    ModificaInserzioneComponent,
    NotificheComponent,
    UserComponent,
    MessageComponent,
  ],
  entryComponents: [
    OffertaComponent,
    ModificaInserzioneComponent,
 ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    MatExpansionModule,
    MatDialogModule,
    MatButtonModule, 
    MatTableModule ,
    MatIconModule,
    MatCheckboxModule, 
    MatFormFieldModule,
    MatPaginatorModule,
    MatInputModule,
    MatSortModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatInputModule,
    MatTableModule,
    MatFormFieldModule ,
    ReactiveFormsModule,
    MatOptionModule,
    MatSelectModule
  ],
  exports: [],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
