import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { SocketioService } from '../services/socketio.service';
@Component({
  selector: 'app-notifiche',
  template: `
    <app-user [daNotificare]=daNotificare></app-child>
  `,
  templateUrl: './notifiche.component.html',
  styleUrls: ['./notifiche.component.css']
})
export class NotificheComponent implements OnInit {

  username = ""
  daNotificare: Number[] = []
  finiteDaNotificare: Number[] = []
  role = ''
  sub
  countAsteVinte = 0
  countAsteFinite = 0
  nuovoMessaggio = false
  constructor(private us: UserService, private router: Router, private sio: SocketioService) { }

  ngOnInit() {
    this.role = this.us.get_role()
    if (this.role === "STUDENTE") {
      this.sub = Observable.interval(10000)
        .subscribe((val) => {
          this.get_aste_da_notificare()
          this.countAsteVinte = this.daNotificare.length
          this.countAsteFinite = this.finiteDaNotificare.length
        });
    }
    this.username = localStorage.username
    this.sio.connect().subscribe((m) => {
      if (this.us.username === m.destinatario) {
        this.nuovoMessaggio = true;
      }
    });
  }


  public get_aste_da_notificare() {
    if (!localStorage.token) {
      //TODO verificare
      /*this.us.get_aste_da_notificare(localStorage.username).subscribe(
         (daNotificare) => {
           this.daNotificare = daNotificare
         }
       );*/
    }
    else {
      this.role = this.us.get_role_with_token(localStorage.token)
      this.us.renew().subscribe(() => {
      })
      this.us.get_aste_da_notificare(localStorage.username).subscribe(
        (daNotificare) => {
          this.daNotificare = daNotificare
        }
      );
      this.us.get_aste_finite_da_notificare(localStorage.username).subscribe(
        (finiteDaNotificare) => {
          this.finiteDaNotificare = finiteDaNotificare
        }
      );
    }
  }

  public goToUserPage() {
    this.router.navigateByUrl('/user');
  }

  deleteNotify() {
    this.nuovoMessaggio = true

  }
}
